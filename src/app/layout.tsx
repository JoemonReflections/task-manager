export const metadata = {
  title: 'Task Manager',
  description: 'Task Manager App',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}
